const fs = require('fs');

let globalArray = [];

getArray();
testArray(globalArray);

function getArray() {
  try {
    const data = fs.readFileSync('task.txt', 'UTF-8');
    const lines = data.split(/\r?\n/);

    lines.forEach((line) => {
      let contentArray = line.split('');
      let numArray = contentArray.map(string => + string);
      
      numArray.unshift(0);
      globalArray.push(numArray);
    });
  } catch (err) {
    console.error(err);
  }
}

function testArray(arr) {
  for (let j = 0; j < arr.length; j++) {
    for (let n = 0; n < arr[j].length; n++) {
      if (!arr[j][n] && (arr[j][n - 1] || arr[j][n + 1])) {
        isTrue = checkArr(arr, j, n);
        if (isTrue) {
          arr[j][n] = 1;
        }
      }
    }
  }
  getKeys(arr);
}

function checkArr(arr, indexGlobal, indexArr) {
  let j = indexGlobal + 1;
  let n = indexArr;
  let checkArr = arr;

  for (let i = 0; i < checkArr.length; i++) {
    if (checkArr[j] === undefined) {
      checkArr[j] = (checkArr[j]) ? checkArr[j] : Array(checkArr[i].length).fill(0);
    } else {
      if (checkArr[j][n]) {
        return true
      }
    }
  }
}

function getKeys(array) {
  let countFigure = 0;
  for (let i = 0; i < array.length; i++) {
    let key1 = (array[i - 1]) ? array[i - 1] : Array(array[i].length).fill(0);
    let key2 = array[i];

    let tempCountFigure = getFigure(key1, key2);

    countFigure = countFigure + tempCountFigure;
  }
  console.log("Общее количество фигур: ", countFigure);
}

function getFigure(preState, currentState) {
  let counter = 0;
  for (let i = 1; i < currentState.length; i++) {

    if (currentState[i]) {
      if (preState[i - 1] || preState[i] || preState[i + 1] || currentState[i - 1]) {
      } else {
        counter++;
      }
    }
  }
  return counter;
}



